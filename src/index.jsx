const React = require('react');
const ReactDOM = require('react-dom');
const {HashRouter, Route, Switch} = require('react-router-dom');

// Styles
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import '../scss/style.scss'
// Temp fix for reactstrap
import '../scss/core/_dropdown-menu-right.scss'

// Containers
const Full = require('./containers/Full/');

// Views
const Login = require('./views/Pages/Login/');
const Register = require('./views/Pages/Register/');
const Page404 = require('./views/Pages/Page404/');
const Page500 = require('./views/Pages/Page500/');

ReactDOM.render((
  <HashRouter>
    <Switch>
      <Route exact path="/login" name="Login Page" component={Login}/>
      <Route exact path="/register" name="Register Page" component={Register}/>
      <Route exact path="/404" name="Page 404" component={Page404}/>
      <Route exact path="/500" name="Page 500" component={Page500}/>
      <Route path="/" name="Home" component={Full}/>
    </Switch>
  </HashRouter>
), document.getElementById('root'));
