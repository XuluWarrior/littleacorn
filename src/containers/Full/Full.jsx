const React = require('react');
const {Component} = React;
const {Switch, Route, Redirect} = require('react-router-dom');
const {Container} = require('reactstrap');
const Header = require('../../components/Header/');
const Sidebar = require('../../components/Sidebar/');
const Breadcrumb = require('../../components/Breadcrumb/');
const Aside = require('../../components/Aside/');
const Footer = require('../../components/Footer/');
const Dashboard = require('../../views/Dashboard/');
const Charts = require('../../views/Charts/');
const Widgets = require('../../views/Widgets/');

// Components
const Buttons = require('../../views/Components/Buttons/');
const Cards = require('../../views/Components/Cards/');
const Forms = require('../../views/Components/Forms/');
const Modals = require('../../views/Components/Modals/');
const SocialButtons = require('../../views/Components/SocialButtons/');
const Switches = require('../../views/Components/Switches/');
const Tables = require('../../views/Components/Tables/');
const Tabs = require('../../views/Components/Tabs/');

// Icons
const FontAwesome = require('../../views/Icons/FontAwesome/');
const SimpleLineIcons = require('../../views/Icons/SimpleLineIcons/');

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              <Switch>
                <Route path="/dashboard" name="Dashboard" component={Dashboard}/>
                <Route path="/components/buttons" name="Buttons" component={Buttons}/>
                <Route path="/components/cards" name="Cards" component={Cards}/>
                <Route path="/components/forms" name="Forms" component={Forms}/>
                <Route path="/components/modals" name="Modals" component={Modals}/>
                <Route path="/components/social-buttons" name="Social Buttons" component={SocialButtons}/>
                <Route path="/components/switches" name="Swithces" component={Switches}/>
                <Route path="/components/tables" name="Tables" component={Tables}/>
                <Route path="/components/tabs" name="Tabs" component={Tabs}/>
                <Route path="/icons/font-awesome" name="Font Awesome" component={FontAwesome}/>
                <Route path="/icons/simple-line-icons" name="Simple Line Icons" component={SimpleLineIcons}/>
                <Route path="/widgets" name="Widgets" component={Widgets}/>
                <Route path="/charts" name="Charts" component={Charts}/>
                <Redirect from="/" to="/dashboard"/>
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

module.exports = Full;
