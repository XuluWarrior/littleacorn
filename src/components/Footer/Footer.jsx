const React = require('react');
const {Component} = React;

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span><a href="http://coreui.io">CoreUI</a> &copy; 2017 creativeLabs.</span>
        <span className="ml-auto">Powered by <a href="http://coreui.io">CoreUI</a></span>
      </footer>
    )
  }
}

module.exports = Footer;
